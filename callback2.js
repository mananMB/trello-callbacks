// Problem 2: Write a function that will return all lists that belong to a board
// based on the boardID that is passed to it from the given data in lists.json.
// Then pass control back to the code that called it by using a callback function.

const fs = require("fs");

const callback2 = (listsFile, boardID, callback) => {
  setTimeout(() => {
    fs.readFile(listsFile, readFileCallback);
  }, 2000);

  const readFileCallback = (error, data) => {
    if (error) {
      if (error.code === "ENOENT") {
        return callback(
          `No such file or directory: ${listsFile.replace(__dirname, "")}`
        );
      }
    } else {
      const listsForBoardID = findListsForBoardID(data);
      if (listsForBoardID === undefined) {
        callback(`No lists found for Board ID: ${boardID}`);
      } else {
        callback(null, listsForBoardID);
      }
    }
  };

  const findListsForBoardID = (data) => {
    return JSON.parse(data.toString())[boardID];
  };
};

module.exports = callback2;
