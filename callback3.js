// Problem 3: Write a function that will return all cards that belong to a
// particular list based on the listID that is passed to it from the given data
// in cards.json. Then pass control back to the code that called it by using a
// callback function.

const fs = require("fs");

const callback3 = (cardsFile, listID, callback) => {
  setTimeout(() => {
    fs.readFile(cardsFile, readFileCallback);
  }, 2000);

  const readFileCallback = (error, data) => {
    if (error) {
      if (error.code === "ENOENT") {
        return callback(
          `No such file or directory: ${cardsFile.replace(__dirname, "")}`
        );
      }
    } else {
      const cardsForListID = getCardsForListID(data);
      if (cardsForListID === undefined) {
        callback(`No cards found for List ID: ${listID}`);
      } else {
        callback(null, cardsForListID);
      }
    }
  };

  const getCardsForListID = (data) => {
    return JSON.parse(data.toString())[listID];
  };
};

module.exports = callback3;
