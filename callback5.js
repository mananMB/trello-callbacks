const callback4 = require("./callback4");

const callback5 = (
  boardFile,
  boardID,
  listFile,
  listNames,
  cardsFile,
  callback
) => {
  callback4(boardFile, boardID, listFile, cardsFile, callback, listNames);
};

module.exports = callback5;
