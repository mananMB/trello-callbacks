const callback6 = require("../callback6");

callback6(
  __dirname + "/../boards.json",
  "Thanos",
  __dirname + "/../lists.json",
  __dirname + "/../cards.json",
  (error, data) => {
    if (error) {
      console.trace(error);
    } else {
      console.log(data);
    }
  }
);
