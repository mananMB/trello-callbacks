const callback1 = require("../callback1");

callback1(__dirname + "/../boards.json", "abc122dc", (error, data) => {
  if (error) {
    console.trace(error);
  } else {
    console.log(data);
  }
});
