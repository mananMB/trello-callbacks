const callback5 = require("../callback5");

callback5(
  __dirname + "/../boards.json",
  "Thanos",
  __dirname + "/../lists.json",
  ["Mind", "Space"],
  __dirname + "/../cards.json",
  (error, data) => {
    if (error) {
      console.trace(error);
    } else {
      console.log(data);
    }
  }
);
