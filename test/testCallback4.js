const callback4 = require("../callback4");

callback4(
  __dirname + "/../boards.json",
  "Thanos",
  __dirname + "/../lists.json",
  __dirname + "/../cards.json",
  (error, data) => {
    if (error) {
      console.trace(error);
    } else {
      console.log(data);
    }
  },
  ["Mind"]
);
