// Problem 4: Write a function that will use the previously written functions
// to get the following information. You do not need to pass control back to
// the code that called it.
//
//      Get information from the Thanos boards
//      Get all the lists for the Thanos board
//      Get all cards for the Mind list simultaneously
const getBoardInfo = require("./callback1");
const getListInfo = require("./callback2");
const getCards = require("./callback3");

const callback4 = (
  boardFile,
  boardID,
  listFile,
  cardsFile,
  callback,
  listNames = []
) => {
  const getCardsForLists = (listIDs) => {
    for (let index = 0; index < listIDs.length; index++) {
      getCards(cardsFile, listIDs[index], (error, data) => {
        if (error) {
          callback(error);
        } else {
          callback(null, data);
        }
      });
    }
  };

  const getListsForBoard = (boardID) => {
    getListInfo(listFile, ...boardID, (error, data) => {
      if (error) {
        callback(error);
      }

      let listIDs;
      if (listNames.length === 0) {
        listIDs = data.map((list) => list.id);
      } else {
        listIDs = data
          .filter((list) => listNames.includes(list.name))
          .map((list) => list.id);
      }
      getCardsForLists(listIDs);
    });
  };

  getBoardInfo(
    boardFile,
    boardID,
    (error, data) => {
      if (error) {
        callback(error);
      } else {
        let boardID = data.map((board) => board.id);
        getListsForBoard(boardID);
      }
    },
    true
  );
};

module.exports = callback4;
