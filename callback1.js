const fs = require("fs");

// Problem 1: Write a function that will return a particular board's information
// based on the boardID that is passed from the given list of boards in boards.json
// and then pass control back to the code that called it by using a callback function.

const callback1 = (boardFile, boardID, callback, findByName = false) => {
  setTimeout(() => {
    fs.readFile(boardFile, readFileCallback);
  }, 2000);

  const readFileCallback = (error, data) => {
    if (error) {
      if (error.code === "ENOENT") {
        return callback(
          `No such file or directory: ${boardFile.replace(__dirname, "")}`
        );
      }
    } else {
      const boardsFound = findBoardIDInBoard(data);
      if (boardsFound.length === 0) {
        callback(`No boards found with IDs: ${boardID}`);
      } else {
        callback(null, boardsFound);
      }
    }
  };

  const findBoardIDInBoard = (data) => {
    if (findByName) {
      return JSON.parse(data.toString()).filter((board) => {
        return boardID.includes(board.name);
      });
    }
    return JSON.parse(data.toString()).filter((board) => {
      return boardID.includes(board.id);
    });
  };
};

module.exports = callback1;
